import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';

// material ui components
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import CircularProgress from '@material-ui/core/CircularProgress';

//icons
import ChevronRight from '@material-ui/icons/ChevronRight';
import SaveIcon from '@material-ui/icons/Save';

//styles
import useStyles from '../forms/styles/wilayah';

// //hooks
import { usePouchDB } from '../../../components/PouchDB/PouchDBProvider';
import { useSnackbar } from 'notistack';

//app components
import { ScrollToTopWithoutRouter } from '../../../components/ScrollToTop';
import { Swipeable } from 'react-swipeable';
import isInt from 'validator/lib/isInt';

// redux implementation
import { withRouter } from "react-router-dom";
import { compose } from "redux";

// axios
import axios from 'axios';
const API_URL = 'http://pk2020-portal-bkkbn.apps.tkp.platform.lintasarta.net/api/laporan/injectRekap';

function Index({history}) {
    const classes = useStyles();
    const nextRef = useRef(null)
    const { user: { metadata }, dataKK, dataKB, dataPK, dataBkkbn } = usePouchDB();
    // console.log(metadata, 'metadata')
    const { enqueueSnackbar } = useSnackbar();
    const [error, setError] = useState({})
    const [wilayah, setWilayah] = useState({})
    const [flags, setFlags] = useState(true)
    const [statusForm, setStatusForm] = useState(true)
    const [statusEdit, setStatusEdit] = useState(true)
    const [isSubmitting, setSubmitting] = useState(false);
    // axios.defaults.headers.post['Content-Type'] ='application/x-www-form-urlencoded';
    const [isSaved, setSaved] = useState({
        local: false,
        remote: false
    })
    

    // PERUBAHAN AMBIL YANG RT RW BKKBN
    useEffect(() => {
        if (!wilayah.hasOwnProperty('no_kk')) {
            setWilayah({
                ...wilayah,
                // ['no_kk'] : Date.now().toString()
                // ['no_kk'] : `${metadata.wil_provinsi.id_provinsi}${metadata.wil_kabupaten.id_kabupaten}${metadata.wil_kecamatan.id_kecamatan}${metadata.wil_kelurahan.id_kelurahan}`
            })
        }

    }, []);
    // END PERUBAHAN AMBIL YANG RT RW BKKBN

    const clearForm = () => {
        setWilayah({
            ...wilayah,
            jumlah_kk: '',
            jumlah_kk_didata: '',
            tidak_ditemui: '',
            ditolak: '',
            tidak_jawab: '',
            yang_ada: ''
        })
    }

    useEffect(() => {
        if (wilayah.id_rw && wilayah.id_rt && flags===true) {
            setFlags(false)
            const id_rw_bkkbn = metadata.wil_rw.find(rw => parseInt(rw.id_rw) === parseInt(wilayah.id_rw)).id_rw_bkkbn
            const id_rt_bkkbn = metadata.wil_rw.find(rw => parseInt(rw.id_rw) === parseInt(wilayah.id_rw)).wil_rt.find(rt => parseInt(rt.id_rt) === parseInt(wilayah.id_rt)).id_rt_bkkbn
            let targetkk = metadata.wil_rw.find(rw => parseInt(rw.id_rw) === parseInt(wilayah.id_rw)).wil_rt.find(rt => parseInt(rt.id_rt) === parseInt(wilayah.id_rt)).targetkk
            let id = `${metadata.wil_provinsi.id_provinsi_depdagri}${metadata.wil_kabupaten.id_kabupaten_depdagri}${metadata.wil_kecamatan.id_kecamatan_depdagri}${metadata.wil_kelurahan.id_kelurahan_depdagri}${id_rw_bkkbn}${id_rt_bkkbn}`
            id = id.replace(' ','');
            console.log(targetkk, 'targetkk')
            const url = `${API_URL}/${id}`;
            console.log(id, 'id')
            axios.get(url)
                .then(function (response) {
                    // handle success
                    console.log(response);
                    if(response && response.data.data && response.data.data.id){
                        console.log("masuk data")
                        setWilayah({
                            ...wilayah,
                            jumlah_kk: response.data.data.jumlah_kk ? response.data.data.jumlah_kk : '0',
                            jumlah_kk_didata: targetkk ? targetkk : '0'
                        })
                        if(response.data.data.status == 2){
                            setStatusForm(true);
                            setStatusEdit(false)
                            setWilayah({
                                ...wilayah,
                                jumlah_kk: response.data.data.jumlah_kk ? response.data.data.jumlah_kk : '0',
                                jumlah_kk_didata: targetkk ? targetkk : '0',
                                tidak_ditemui: response.data.data.tidak_ditemui ? response.data.data.tidak_ditemui : '0',
                                ditolak: response.data.data.ditolak ? response.data.data.ditolak : '0',
                                tidak_jawab: response.data.data.tidak_jawab ? response.data.data.tidak_jawab : '0',
                                yang_ada: response.data.data.yang_ada ?  response.data.data.yang_ada : '0'
                            })
                            let message = `Data rekap telah di submit`
                            enqueueSnackbar(message, { variant: "info" })
                        }else{
                            setStatusForm(false)
                            clearForm()
                            setWilayah({
                                ...wilayah,
                                jumlah_kk: response.data.data.jumlah_kk ? response.data.data.jumlah_kk : '0',
                                jumlah_kk_didata: targetkk ? targetkk : '0'
                            })
                        }
                    }else{
                        console.log("else data kosong")
                        setStatusForm(true)
                        clearForm()
                        let message = `Jumlah KK belum dimasukkan oleh supervisor`
                        enqueueSnackbar(message, { variant: "warning" })
                        setWilayah({
                            ...wilayah,
                            jumlah_kk: response.data.data.jumlah_kk ? response.data.data.jumlah_kk : '0',
                            jumlah_kk_didata: targetkk ? targetkk : '0'
                        })
                    }
                })
                .catch(function (error) {
                    let message = `Jaringan bermasalah`
                    enqueueSnackbar(message, { variant: "warning" })
                    setStatusForm(true)
                    clearForm()
                })
        }
    }, [wilayah])

    const handleChange = (e) => {
        if (e.target.name === 'id_rt'){
            setFlags(true)
        }

        if (e.target.type === "number") {
            if (parseInt(e.target.value) < 0)
                return false;
        }
        setWilayah({
            ...wilayah,
            [e.target.name]: e.target.value
        })
        setError({
            ...error,
            [e.target.name]: ""
        })
    }

    const validate = () => {
        let newError = {};

        if (!wilayah.id_rw) {
            newError.id_rw = "RW/Dusun wajib diisi";
        }

        if (!wilayah.id_rt) {
            newError.id_rt = "RT wajib diisi";
        }

        if (!wilayah.jumlah_kk) {
            newError.jumlah_kk = "Jumlah KK wajib diisi";
        }

        if (!wilayah.jumlah_kk_didata) {
            newError.jumlah_kk_didata = "Jumlah KK yang didata wajib diisi";
        }

        if (!wilayah.tidak_ditemui) {
            newError.tidak_ditemui = "Jumlah KK yang tidak dapat ditemui diisi";
        }

        if (!wilayah.ditolak) {
            newError.ditolak = "Jumlah KK yang ditolak wajib diisi";
        }

        if (!wilayah.tidak_jawab) {
            newError.tidak_jawab = "Jumlah KK yang tidak mampu menjawab wajib diisi";
        }

        if (!wilayah.yang_ada) {
            newError.yang_ada = "Jumlah KK yang ada wajib diisi";
        }
        
        return newError;

    }

    const handleUbah = () => {
        setStatusForm(false);
        setStatusEdit(true);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const findErrors = validate();

        const errorValues = Object.values(findErrors);

        if (errorValues.length > 0 && errorValues.some(err => err !== '')) {
            setError(findErrors);
        } else {
            let id_rw_bkkbn = metadata.wil_rw.find(rw => parseInt(rw.id_rw) === parseInt(wilayah.id_rw)).id_rw_bkkbn
            let id_rt_bkkbn = metadata.wil_rw.find(rw => parseInt(rw.id_rw) === parseInt(wilayah.id_rw)).wil_rt.find(rt => parseInt(rt.id_rt) === parseInt(wilayah.id_rt)).id_rt_bkkbn
            let id = `${metadata.wil_provinsi.id_provinsi_depdagri}${metadata.wil_kabupaten.id_kabupaten_depdagri}${metadata.wil_kecamatan.id_kecamatan_depdagri}${metadata.wil_kelurahan.id_kelurahan_depdagri}${id_rw_bkkbn}${id_rt_bkkbn}`
            id = id.replace(' ','')
            const jumlah_kk = parseInt(wilayah.jumlah_kk)
            const jumlah_kk_didata = parseInt(wilayah.jumlah_kk_didata)
            const tidak_ditemui = parseInt(wilayah.tidak_ditemui)
            const ditolak = parseInt(wilayah.ditolak)
            const tidak_jawab = parseInt(wilayah.tidak_jawab)
            const yang_ada = parseInt(wilayah.yang_ada)
            const url = `${API_URL}/${id}`;
            // console.log(url, 'url submit')
            axios.post(url, {
                    jumlah_kk: jumlah_kk,
                    jumlah_kk_didata: jumlah_kk_didata,
                    tidak_ditemui: tidak_ditemui,
                    ditolak: ditolak,
                    tidak_jawab: tidak_jawab,
                    yang_ada: yang_ada
                })
                .then(function (response) {
                    // handle success
                    let message = `Simpan Berhasil`
                    enqueueSnackbar(message, { variant: "success" })
                    setStatusForm(true)
                    setStatusEdit(false)
                })
                .catch(function (error) {
                    console.log(error)
                    let message = `Jaringan Bermasalah`
                    enqueueSnackbar(message, { variant: "error" })
                })
        }
    }

    return (
        <div>
            <form onSubmit={handleSubmit} className={classes.form}>
                <ScrollToTopWithoutRouter />
                <Grid container spacing={3}>

                    <Grid item xs={12} className={classes.textCenter}>
                        <Typography variant="h5" component="h1" style={{marginTop: '5px'}}>Rekap RT</Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Divider />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Typography>Provinsi: {metadata.wil_provinsi.nama_provinsi}</Typography>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Typography>Kabupaten/Kota: {metadata.wil_kabupaten.nama_kabupaten}</Typography>

                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Typography>Kecamatan: {metadata.wil_kecamatan.nama_kecamatan}</Typography>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Typography>Desa/Kel: {metadata.wil_kelurahan.nama_kelurahan}</Typography>
                    </Grid>

                    <Grid item xs={12}>
                        <Divider />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <FormControl
                            disabled={isSubmitting}
                            variant="outlined" fullWidth error={error.id_rw ? true : false}>

                            <Select
                                id="id_rw"
                                value={wilayah.id_rw || ''}
                                onChange={handleChange}
                                name="id_rw"
                                displayEmpty
                            >
                                <MenuItem value="">Pilih RW/Dusun</MenuItem>

                                {metadata.wil_rw.map(rw => <MenuItem key={rw.id_rw} value={rw.id_rw}>{rw.nama_rw}</MenuItem>)}
                            </Select>
                            <FormHelperText>{error.id_rw}</FormHelperText>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <FormControl
                            disabled={isSubmitting}
                            variant="outlined" fullWidth error={error.id_rt ? true : false}>

                            <Select
                                id="id_rt"
                                value={wilayah.id_rt || ''}
                                onChange={handleChange}
                                name="id_rt"
                                displayEmpty
                            >
                                <MenuItem value="">Pilih RT</MenuItem>
                                {wilayah.id_rw &&
                                metadata.wil_rw.find(rw => parseInt(rw.id_rw) === parseInt(wilayah.id_rw)).wil_rt.map(rt => <MenuItem key={rt.id_rt} value={rt.id_rt}>{rt.nama_rt}</MenuItem>)}


                            </Select>
                            <FormHelperText>{error.id_rt}</FormHelperText>
                        </FormControl>

                    </Grid>

                    <Grid item xs={12} md={2}>
                        Target KK
                    </Grid>
                    <Grid item xs={12} md={4}>
                        <TextField
                            disabled
                            fullWidth
                            variant="outlined"
                            value={wilayah.jumlah_kk || ''}
                            onChange={handleChange}
                            name="jumlah_kk"
                            type="number"
                            placeholder="Jumlah KK"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <FormHelperText>{error.jumlah_kk}</FormHelperText>
                    </Grid>

                    <Grid item xs={12} md={2}>
                        Jumlah KK Yang Didata
                    </Grid>
                    <Grid item xs={12} md={4}>
                        <TextField
                            disabled
                            fullWidth
                            variant="outlined"
                            value={wilayah.jumlah_kk_didata || ''}
                            onChange={handleChange}
                            name="jumlah_kk_didata"
                            type="number"
                            placeholder="Masukkan Jumlah KK Yang Didata"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <FormHelperText>{error.jumlah_kk_didata}</FormHelperText>
                    </Grid>
                    
                    <Grid item xs={12} md={2}>
                        Jumlah KK Yang Tidak Dapat Ditemui
                    </Grid>
                    <Grid item xs={12} md={4}>
                        <TextField
                            disabled={statusForm}
                            fullWidth
                            variant="outlined"
                            value={wilayah.tidak_ditemui || ''}
                            onChange={handleChange}
                            name="tidak_ditemui"
                            type="number"
                            placeholder="Masukkan Jumlah KK Yang Tidak Dapat Ditemui"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <FormHelperText>{error.tidak_ditemui}</FormHelperText>
                    </Grid>

                    <Grid item xs={12} md={2}>
                        Jumlah KK Yang Ditolak
                    </Grid>
                    <Grid item xs={12} md={4}>
                        <TextField
                            disabled={statusForm}
                            fullWidth
                            variant="outlined"
                            value={wilayah.ditolak || ''}
                            onChange={handleChange}
                            name="ditolak"
                            type="number"
                            placeholder="Masukkan Jumlah KK Yang Ditolak"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <FormHelperText>{error.ditolak}</FormHelperText>
                    </Grid>

                    <Grid item xs={12} md={2}>
                        Jumlah KK Yang Tidak Mampu Menjawab
                    </Grid>
                    <Grid item xs={12} md={4}>
                        <TextField
                            disabled={statusForm}
                            fullWidth
                            variant="outlined"
                            value={wilayah.tidak_jawab || ''}
                            onChange={handleChange}
                            name="tidak_jawab"
                            type="number"
                            placeholder="Masukkan Jumlah KK Yang tidak Mampu Menjawab"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <FormHelperText>{error.tidak_jawab}</FormHelperText>
                    </Grid>

                    <Grid item xs={12} md={2}>
                        Jumlah KK Yang Ada
                    </Grid>
                    <Grid item xs={12} md={4}>
                        <TextField
                            disabled={statusForm}
                            fullWidth
                            variant="outlined"
                            value={wilayah.yang_ada || ''}
                            onChange={handleChange}
                            name="yang_ada"
                            type="number"
                            placeholder="Masukkan Jumlah KK Yang Ada"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <FormHelperText>{error.yang_ada}</FormHelperText>
                    </Grid>

                    <Grid item xs={12} md={6} > 
                        <Button 
                            disabled={statusEdit}
                            onClick={handleUbah}
                            variant="contained" color="primary" style={{width: '-webkit-fill-available'}}>
                            Ubah                        
                        </Button>
                    </Grid>

                    <Grid item xs={12} md={6} > 
                        <Button 
                            disabled={statusForm}
                            onClick={handleSubmit}
                            variant="contained" color="primary" style={{width: '-webkit-fill-available'}}>
                            Simpan                        
                        </Button>
                    </Grid>

                </Grid>

            </form>
        </div>
    )
}

export default Index;